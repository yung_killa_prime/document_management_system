<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('name');
            $table->string('state');
            $table->date('estimated_completion_date');
            $table->bigInteger('initial_price');
            $table->bigInteger('tender_id');
            $table->bigInteger('initial_price_with_vat')->nullable();
            $table->string('pricing_procedure_with_vat')->nullable();
            $table->foreign('tender_id')->references('id')->on('tender')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract', function (Blueprint $table) {
            $table->dropForeign(['tender_id']);
            $table->dropColumn(['tender_id']);
        });
        Schema::dropIfExists('contract');
    }
}
