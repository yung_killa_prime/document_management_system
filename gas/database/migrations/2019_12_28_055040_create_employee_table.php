<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('education_id');
            $table->bigInteger('qualification_id');
            $table->bigInteger('employee_position_id');
            $table->bigInteger('academic_degree_id');
            $table->tinyInteger('work_experience');
            $table->bigInteger('user_id');
            $table->foreign('education_id')->references('id')->on('education')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('qualification_id')->references('id')->on('qualification')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('employee_position_id')->references('id')->on('employee_position')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('academic_degree_id')->references('id')->on('academic_degree')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->dropForeign(['education_id']);
            $table->dropForeign(['qualification_id']);
            $table->dropForeign(['employee_position_id']);
            $table->dropForeign(['academic_degree_id']);
            $table->dropForeign(['user_id']);
            $table->dropColumn(['education_id']);
            $table->dropColumn(['qualification_id']);
            $table->dropColumn(['employee_position_id']);
            $table->dropColumn(['academic_degree_id']);
            $table->dropColumn(['user_id']);
        });
        Schema::dropIfExists('employee');
    }
}
