<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_card', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('registration_number');
            $table->date('date_of_registration');
            $table->bigInteger('tender_id');
            $table->bigInteger('organization_id');
            $table->bigInteger('individual_id');
            $table->bigInteger('register_card_application_id');
            $table->bigInteger('person_in_charge');
            $table->bigInteger('key_member_staff');
            $table->bigInteger('customer');
            $table->bigInteger('organizer');
            $table->foreign('tender_id')->references('id')->on('tender')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('organization_id')->references('id')->on('organization')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('individual_id')->references('id')->on('individual')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('register_card_application_id')->references('id')->on('register_card_application')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('person_in_charge')->references('id')->on('employee')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('key_member_staff')->references('id')->on('employee')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('customer')->references('id')->on('organization')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('organizer')->references('id')->on('organization')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_card', function (Blueprint $table) {
            $table->dropForeign(['tender_id']);
            $table->dropForeign(['organization_id']);
            $table->dropForeign(['individual_id']);
            $table->dropForeign(['register_card_application_id']);
            $table->dropForeign(['person_in_charge']);
            $table->dropForeign(['key_member_staff']);
            $table->dropForeign(['customer']);
            $table->dropForeign(['organizer']);
            $table->dropColumn(['tender_id']);
            $table->dropColumn(['organization_id']);
            $table->dropColumn(['individual_id']);
            $table->dropColumn(['register_card_application_id']);
            $table->dropColumn(['person_in_charge']);
            $table->dropColumn(['key_member_staff']);
            $table->dropColumn(['customer']);
            $table->dropColumn(['organizer']);
        });
        Schema::dropIfExists('register_card');
    }
}
