<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterCardApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_card_application', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('commercial_offer');
            $table->string('technical_offer');
            $table->string('member_profile')->nullable();
            $table->string('material_and_technical_resources_certificate');
            $table->string('subcontractor_info')->nullable();
            $table->string('work_schedule_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_card_application');
    }
}
