<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tender', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('lot_number');
            $table->string('subject_of_tender');
            $table->date('start_of_proposal_collection');
            $table->date('end_of_proposal_collection');
            $table->bigInteger('lot_type_id');
            $table->foreign('lot_type_id')->references('id')->on('lot_type')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tender', function (Blueprint $table) {
            $table->dropForeign(['lot_type_id']);
            $table->dropColumn(['lot_type_id']);
        });
        Schema::dropIfExists('tender');
    }
}
